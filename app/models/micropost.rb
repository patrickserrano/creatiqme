class Micropost < ActiveRecord::Base
  attr_accessible :content, :image, :image_file_name, :image_content_type, 
                  :image_file_size, :image_updated_at  
  belongs_to :user
  
  has_attached_file :image, :styles => {
                            :small => "250x250>",
                            :medium => "500x500>", 
                            :large => "1000x1000>"
                            },
                            :storage => :s3,
                            :bucket => ENV['S3_BUCKET_NAME'],
                            :s3_credentials => {
                                               :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
                                               :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
                            },
                            :path => "/:style/:id/:filename"
    
  validates :content, presence: true, length: { maximum: 255 }
  validates :user_id, presence: true
  validates_attachment_presence :image, presence: true
  # validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf']
  
  default_scope order: 'microposts.created_at DESC'
end